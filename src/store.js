import Vue from 'vue'
import Vuex from 'vuex'
import { ADD_NEW_NOTE, DELETE_ALL_NOTES } from './mutation-types'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    notes: []
  },
  getters: {
    firstFiveNotes: state => {
      return state.notes.slice(0, 5)
    }
  },
  mutations: {
    [ADD_NEW_NOTE] (state, note) {
      state.notes.push(note)
    },
    [DELETE_ALL_NOTES] (state) {
      state.notes = []
    }
  },
  actions: {
    addNote (context, note) {
      context.commit(ADD_NEW_NOTE, note)
    },
    deleteAllNotes (context) {
      context.commit(DELETE_ALL_NOTES)
    }
  }
})
